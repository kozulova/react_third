import React from 'react'

const Preloader = () => {
    return (
        <div>
            <img src= {process.env.PUBLIC_URL + '/load.gif'} alt='Preloader'/>
        </div>
    )
}

export default Preloader
