import React from 'react'
import moment from 'moment'

const OwnMessage = ({message, deleteMessage}) => {
    return (
        <div className="own-message">
            <div className="message-text">{message.text}</div>
            <div className="message_info" id={message.id}>
            <div className="message-time">{moment(message.createdAt).format("hh:mm")}</div>
            <button className="message-edit">Edit</button>
            <button className="message-delete" onClick={(e)=>deleteMessage(e.target.parentElement.id)}>Delete</button>
            </div>
        </div>
    )
}

export default OwnMessage
