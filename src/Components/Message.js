import React from 'react'
import moment from 'moment'

const Message = ({message, likeMessage}) => {
    const like = () => {

    }
    return (
        <div className="message">
            <img src={message.avatar} alt="" className="message-user-avatar"/>
            <div>
            <div className="message-text">{message.text}</div>
            <div className="message_info">
            <div className="message-time">{moment(message.createdAt).format("hh:mm")}</div>
            {message.user !== 'Me' && <button className='message-like' onClick={()=>likeMessage(message.id)}>Like</button>}
            <div className="message-user-name">{message.user}</div>
            </div>
            </div>
        </div>
    )
}

export default Message;
