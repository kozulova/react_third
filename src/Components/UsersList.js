import React, {useEffect} from 'react'
import { checkLogin, getUsers } from '../store/actions'
import { useDispatch, useSelector } from 'react-redux';
import {Redirect} from 'react-router-dom';

const UsersList = () => {
    const {user, users} = useSelector(state => ({
        ...state.user,
        ...state.users
    }));
    const dispatch = useDispatch();  
    useEffect(() => {
        dispatch(checkLogin());
        dispatch(getUsers());
    }, [])
    const renderUsers = (listOfUsers) => {
        console.log(listOfUsers);
        return <div>{listOfUsers.map(u => <div key={u.id}>{u.user} <button>Edit</button> <button>Delete</button></div>)}</div>
    }
    return (
        <div>
        {user && user.role !=='admin' && <Redirect to="/chat" /> }
            <h2>users page</h2>
            {users && renderUsers(users)}
        </div>
    )
}

export default UsersList
