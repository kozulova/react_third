import { createReducer } from '@reduxjs/toolkit';
import { setMessages, setUser, setUsers } from './actions';

const initialState = {
    messages: [],
    user: null,
    users: []
}

const reducer = createReducer(initialState, builder => {
    builder.addCase(setMessages, (state, action) => {
        const {messages} = action.payload;
        state.messages = messages;
    });
    builder.addCase(setUser, (state, action)=> {
        state.user = action.payload;
    });
    builder.addCase(setUsers, (state, action)=>{
        state.users = action.payload;
    })
})


export {reducer};