import { createAction } from '@reduxjs/toolkit';

const setMessages = createAction('SET_MESSAGES', messages => ({
    payload: {
        messages
    }
}))

const setUser = createAction('SET_USER', user => ({
    payload: {
        user
    }
}))

const setUsers = createAction('SET_USERS', users => ({
    payload: {
        users
    }
}))

const loadMessages = (url) => async dispatch => {
    const response = await fetch(url);
    const messages = await response.json();
    console.log(messages);
    dispatch(setMessages(messages));
}

const checkLogin = (user) => async dispatch => {
    let currentUser = null;
    if(user){
        const response = await fetch('http://localhost:3001/users');
        const users = await response.json();
        currentUser = users.find(u => u.user == user);
        currentUser && localStorage.setItem('user', JSON.stringify(currentUser));
        console.log(currentUser);
    }else{
        currentUser = JSON.parse(localStorage.getItem('user'));
    }   
    currentUser ? dispatch(setUser(currentUser)) : console.log('No such user');
}

const getUsers = () => async dispatch => {
    const response = await fetch('http://localhost:3001/users');
    const users = await response.json();
    dispatch(setUsers(users));
}

export {
    loadMessages,
    setMessages,
    checkLogin,
    setUser,
    getUsers,
    setUsers
};