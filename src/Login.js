import React, {useEffect} from 'react'
import { checkLogin } from './store/actions';
import { useDispatch, useSelector } from 'react-redux';
import {Redirect} from 'react-router-dom';

const Login = () => {
    const {user} = useSelector(state => ({
        user: state.user
    }));
    const dispatch = useDispatch();    
    useEffect(() => {
       dispatch(checkLogin());   
    }, [])

    const loginUser = (event) => {
        event.preventDefault();
        console.log(event.target.username.value);
        dispatch(checkLogin(event.target.username.value));
      };

    return (
        <div>
        {user &&  <Redirect to="/chat" /> }
            <form action="" onSubmit={loginUser}>
                <input type="text" placeholder="name" id="username"/>
                <button type="submit">Login</button>
            </form>
        </div>
    )
}

export default Login
