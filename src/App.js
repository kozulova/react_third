import './App.css';
import Chat from './Chat';
import Login from './Login';
import UsersList from './Components/UsersList'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/chat">
            <Chat url='http://localhost:3001/messages' />
          </Route>
          <Route path="/users">
            <UsersList/>
          </Route>
          <Route path="/">
            <Login />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
